<?php

namespace Samwilson\MwCliSamwilson\Command;

use Addwiki\Mediawiki\Api\Client\Action\Request\ActionRequest;
use Addwiki\Mediawiki\Api\Client\MediaWiki;
use DateTime;
use DateTimeZone;
use Samwilson\MediaWikiCLI\Command\CommandBase;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class PopulationForWikidataCommand extends CommandBase {

    /**
     * Configure this command.
     */
    public function configure() {
        parent::configure();
        $this->setName( 'population-for-wikidata' );
        $this->setDescription( 'Compile and upload stats for the population project.' );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    public function execute( InputInterface $input, OutputInterface $output ) {
        parent::execute($input, $output);
        $wikipediaApi = MediaWiki::newFromEndpoint( 'https://en.wikipedia.org/w/api.php' )->action();

        // Collect new data.
        $withoutPop = $this->searchCount( $wikipediaApi, "insource:\"Infobox Australian place\" -insource:/pop *=/" );
        $withEmptyPop = $this->searchCount( $wikipediaApi, "insource:\"Infobox Australian place\" insource:/pop *= *[^0-9,] *\|/" );
        $withNonEmptyPop = $this->searchCount( $wikipediaApi, "insource:\"Infobox Australian place\" insource:/pop *= *[0-9,]+/" );
        $res = $wikipediaApi->request(ActionRequest::simpleGet(
            'query',
            [
                "format" => "json",
                "prop" => "categoryinfo",
                "list" => "",
                "meta" => "",
                "titles" => "Category:Australian place articles using Wikidata population values",
                "cicontinue" => ""
            ] ) );
        $catInfo = array_shift($res['query']['pages']);
        $withWikidata = $catInfo['categoryinfo']['size'];

        $date = (new DateTime())->setTimezone(new DateTimeZone('Z'))->format('Y-m-d H:i:s');
        $this->io->writeln( sprintf( ', [ "%s", %s, %s, %s, %s ]', $date, $withWikidata, $withNonEmptyPop, $withEmptyPop, $withoutPop ) );
        return Command::SUCCESS;

        // // Get the existing data.
        // $commonsDataTitle = 'Data:PopulationFromWikidata_stats.tab';
        // $commonsApi = MediaWiki::newFromEndpoint( 'https://commons.wikimedia.org/w/api.php' )->action();
        // $data = $commonsApi->request( ActionRequest::factory()->setAction( 'query' )
        //     ->addParams([
        //         'prop' => 'revisions',
        //         'titles' => $commonsDataTitle,
        //         'rvslots' => '*',
        //         'rvprop' => 'content|ids',
        //     ])
        // );
        // $page = array_shift($data['query']['pages']);
        // $pageData = json_decode($page['revisions'][0]['slots']['main']['*']);

        // // Save the new data.
        // $date = (new DateTime())->setTimezone(new DateTimeZone('Z'))->format('Y-m-d H:i:s');
        // $pageData->data[] = [ $date, $withWikidata, $withNonEmptyPop, $withEmptyPop, $withoutPop ];

        // // $samApi = MediawikiApi::newFromApiEndpoint( 'https://wiki.samwilson.id.au/api.php' );
        // // $samApi->login(new ApiUser('sam', ''));
        // $site = $this->getSite( $input );
        // dd($site);
        // $site->postRequest( ActionRequest::factory()->setAction( 'edit' )
        //     ->setParam( 'title', $commonsDataTitle )
        //     ->setParam( 'text', json_encode( $pageData ) )
        //     ->setParam( 'summary', 'Update with current search results.' )
        //     ->setParam( 'token', $samApi->getToken() )
        //    // ->setParam( 'baserevid', $page['revisions'][0]['revid'] )
        // );

        // return 0;
    }

    private function searchCount($wikipediaApi, $search): int {
        $res = $wikipediaApi->request(ActionRequest::simpleGet(
            'query',
            [
                "action" => "query",
                "format" => "json",
                "list" => "search",
                "srsearch" => $search,
            ]));
        return (int)$res['query']['searchinfo']['totalhits'];
    }
}
