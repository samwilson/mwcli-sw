<?php

namespace Samwilson\MwCliSamwilson\Command;

use GuzzleHttp\Client;
use Samwilson\MediaWikiCLI\Command\CommandBase;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class WikiTreeLGAsCommand extends CommandBase {

	public function configure() {
		parent::configure();
		$this->setName( 'wikitree:lgas' );
		$this->setDescription( 'Create wikitext for Space:Western_Australia_Local_Government_Directory table' );
	}

	/**
	 * @inheritDoc
	 */
	public function execute( InputInterface $input, OutputInterface $output ) {
		parent::execute( $input, $output );
		$query = 'SELECT ?item ?itemLabel ?sitelink ?spaceOrCat WHERE {
			?item wdt:P31 wd:Q55557858 .
			OPTIONAL { ?item wdt:P7607 ?spaceOrCat } .
			MINUS { ?item wdt:P576 ?demolished } .
			OPTIONAL {
				?sitelink schema:about ?item .
				?sitelink schema:isPartOf <https://en.wikipedia.org/> .
			}
			SERVICE wikibase:label { bd:serviceParam wikibase:language "en" }
		} ORDER BY ?sitelink';
		$url = 'https://query.wikidata.org/bigdata/namespace/wdq/sparql?format=json&query=' . urlencode( $query );
		$httpClient = new Client();
		$response = $httpClient->get( $url );
		$results = json_decode( $response->getBody()->getContents() );

		$data = [];
		foreach ( $results->results->bindings as $res ) {
			$name = $res->itemLabel->value;
			$wikidata = substr( $res->item->value, strlen( 'http://www.wikidata.org/entity/' ) );
			$wikipedia = str_replace( '_', ' ', urldecode( substr( $res->sitelink->value, strlen( 'https://en.wikipedia.org/wiki/' ) ) ) );
			$space = '';
			$cat = '';
			if ( isset( $res->spaceOrCat->value ) ) {
				$spaceOrCat = str_replace( '_', ' ', $res->spaceOrCat->value );
				if ( str_starts_with( $spaceOrCat, 'Space:' ) ) {
					$space = '[[' . $spaceOrCat . '|' . substr( $spaceOrCat, strlen( 'Space:' ) ) . ']]';
				} elseif ( str_starts_with( $spaceOrCat, 'Category:' ) ) {
					$cat = '[[:' . $spaceOrCat . '|' . substr( $spaceOrCat, strlen( 'Category:' ) ) . ']]';
				} else {
					$this->io->error( 'Is not a space or category: ' . $spaceOrCat );
				}
			}
			$data[$wikidata] = [
				'name' => $name,
				'wikidata' => $wikidata,
				'space' => isset( $data[$wikidata]['space'] ) && $data[$wikidata]['space'] ? $data[$wikidata]['space'] : $space,
				'cat' => isset( $data[$wikidata]['cat'] ) && $data[$wikidata]['cat'] ? $data[$wikidata]['cat'] : $cat,
				'wikipedia' => $wikipedia,
			];
		}

		$filename = dirname( __DIR__, 2 ) . '/wikitree-lgas.txt';
		$outFile = fopen( $filename, 'w' );
		fputs( $outFile, "{| border='1'\n! LGA !! Space !! Category !! Wikidata !! Wikipedia\n" );
		foreach ( $data as $datum ) {
			fputs(
				$outFile,
				"|-\n"
				. '| ' . $datum['name']
				. ' || ' . $datum['space']
				. ' || ' . $datum['cat']
				. ' || {{wikidata|' . $datum['wikidata'] . '}}'
				. ' || [[wikipedia:' . $datum['wikipedia'] . '|' . $datum['wikipedia'] . ']]'
				. "\n"
			);
		}
		fputs( $outFile, "|}\n" );
		$this->io->success( 'Written to ' . $filename );
		return Command::SUCCESS;
	}
}
