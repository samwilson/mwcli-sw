<?php

namespace Samwilson\MwCliSamwilson\Command;

use Addwiki\Mediawiki\Api\Client\Action\Request\ActionRequest;
use Addwiki\Mediawiki\Api\Client\MediaWiki;
use Samwilson\MediaWikiCLI\Command\CommandBase;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class WikisourceAuthorInitialsCommand extends CommandBase {

	/**
	 * Configure this command.
	 */
	public function configure() {
		$this->setName( 'wikisource-author-initials' );
		$this->setDescription( "Get information about Wikisource authors' family names and initial letters." );
	}

	/**
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 * @return int
	 */
	public function execute( InputInterface $input, OutputInterface $output ) {
		parent::execute( $input, $output );

        $apiWikisource = MediaWiki::newFromEndpoint( 'https://en.wikisource.org/w/api.php' )->action();
        $apiWikidata = MediaWiki::newFromEndpoint( 'https://www.wikidata.org/w/api.php' )->action();

		$outFile = fopen( __DIR__ . '/../../data/last_initials.csv', 'w' );
		fputcsv($outFile, [ 'page_title', 'last_initial_param', 'lastname_param', 'wikidata', 'family_name', 'family_name_initial', 'initials_match' ] );

		$requestParams = [
			"prop" => "revisions",
			"continue" => "gapcontinue||",
			"generator" => "allpages",
			"formatversion" => "2",
			"rvprop" => "content",
			"rvslots" => "main",
			"gapnamespace" => "102", // Author NS
			"gapfilterredir" => "nonredirects",
			'gaplimit' => '50',
		];
		$propFamilyName = 'P734';
		do {
			if ( isset( $result['continue']['gapcontinue'] ) ) {
				$requestParams['gapcontinue'] = $result['continue']['gapcontinue'];
			}

			$result = $apiWikisource->request( ActionRequest::simpleGet( 'query', $requestParams ) );
			if ( !isset( $result['query']['pages'] ) ) {
				continue;
			}

			$titles = [];
			$lastinitialParams = [];
			$lastnameParams = [];
			foreach ( $result['query']['pages'] as $page ) {
				$titles[] = $page['title'];
				// if (!isset($page['revisions'][0]['slots']['main']['content'] )) {
				// 	// Might happen if not authenticated?
				// 	dd($page);
				// }
				$content = $page['revisions'][0]['slots']['main']['content'];

				preg_match('/lastname *= *([^\|}]+)/i', $content, $matches1);
				$lastnameParams[$page['title']] = isset( $matches1[1] ) ? trim( $matches1[1]) : '';

				preg_match('/last_initial *= *([^\|}]+)/i', $content, $matches2);
				if ( isset( $matches2[1] ) ) {
					$lastinitialParams[$page['title']] = trim( $matches2[1] );
				} else {
					$output->writeln( 'No last_initial for ' . $page['title'] );
				}
			}

			$entities = $apiWikidata->request( ActionRequest::simpleGet( 'wbgetentities', [
				'props' => 'claims|sitelinks',
				"sites" => "enwikisource",
				"titles" => join( '|', $titles ),
				"formatversion" => '2',
			] ) );
			foreach ($entities['entities'] as $entity ) {
				$familyNameLabel = '';
				if ( !isset( $entity['sitelinks']['enwikisource']['title'] ) ) {
					$title = $entity['title'];
					$lastInitial = $lastinitialParams[$title] ?? '';
					$output->writeln( "No Wikidata item found for $title" );
				} else {
					$title = $entity['sitelinks']['enwikisource']['title'];
					$lastInitial = $lastinitialParams[$title] ?? '';
					if ( !isset( $entity['claims'][ $propFamilyName ][0]['mainsnak']['datavalue']['value']['id'] ) ) {
						$output->writeln( 'No family name for "' . $title . '" https://www.wikidata.org/wiki/' . $entity['id'] );
					} else {
						$familyNameId = $entity['claims'][ $propFamilyName ][0]['mainsnak']['datavalue']['value']['id'];
						$familyName = $apiWikidata->request( ActionRequest::simpleGet( 'wbgetentities', [ 'props' => 'labels', 'ids' => $familyNameId ] ) );
						$familyNameLabel = $familyName['entities'][$familyNameId]['labels']['en']['value'];
						$output->writeln("$title has family name '$familyNameLabel' ($lastInitial)");
					}
				}
				$calculatedInitial = mb_substr( $familyNameLabel, 0, 2 );
				fputcsv( $outFile, [
					$title,
					$lastInitial,
					$lastnameParams[$title],
					$entity['id'] ?? '',
					$familyNameLabel,
					$calculatedInitial,
					$lastInitial === $calculatedInitial ? '1' : 0,
				] );
			}

		} while ( isset( $result['continue'] ) );

		/*
		Total number of Author pages:
		No value for last_initial: 
		Not connected to Wikidata:
		Wikidata item has no 'family name' property:
		Existing last_initial matches first two characters of 'family name' property:
		Does not match:
		*/

		return Command::SUCCESS;
	}
}
